/**
 * Created by Ian on 11/28/2016.
 */
public class f {

    public int f(int x, int y) {
        int a = x + 1;
        if (x == 0) {
            return g(x,y,y);
        } else {
            return a + g(x,y,f((x - 1), y));
        }
    }

    public int g(int x, int y, int n) {
        int a = x + y;
        if (n > 0){
            a = h(a, x, y, n - 1);
        }
        return a;
    }

    public int h(int a, int x, int y, int k) {
        if (k > 0){
            k = a + (k + 1) + g(x,y,k);
        }
        return k;
    }

    public static void main(String[] args) {
        f obj = new f();
        System.out.println(obj.f(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
    }
}